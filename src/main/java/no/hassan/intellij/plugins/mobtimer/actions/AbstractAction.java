package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import java.awt.event.ActionListener;

import static com.intellij.icons.AllIcons.General.QuestionDialog;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_CANCEL;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.BTN_OK;
import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.TITLE_ERROR;

public abstract class AbstractAction implements ActionListener {

  protected final Project project;
  protected final MobbingManager manager;

  protected AbstractAction(@NotNull Project project, @NotNull MobbingManager manager) {
    this.project = project;
    this.manager = manager;
  }

  void showInfoMessage(String title, String message) {
    Messages.showInfoMessage(project, message, title);
  }

  int showOkCancelDialog(String message) {
    return Messages.showOkCancelDialog(
        project, message, TITLE_ERROR, BTN_OK, BTN_CANCEL, QuestionDialog);
  }
}
