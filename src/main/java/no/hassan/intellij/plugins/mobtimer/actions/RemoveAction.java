package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.Mobber;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import javax.swing.JTable;
import java.awt.event.ActionEvent;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_REMOVE;

public class RemoveAction extends AbstractAction {

  private JTable mobbersTable;

  public RemoveAction(
      @NotNull Project project,
      @NotNull MobbingManager mobbingManager,
      @NotNull JTable mobbersTable) {
    super(project, mobbingManager);
    this.mobbersTable = mobbersTable;
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int selectedRow = mobbersTable.getSelectedRow();
    if (selectedRow > -1 && selectedRow < manager.getMobbers().size()) {
      Mobber mobber = manager.getMobber(selectedRow);
      int nowWhat = showOkCancelDialog(String.format(MSG_REMOVE, mobber.getName()));
      if (nowWhat == Messages.OK) {
        manager.removeMobber(selectedRow);
      }
    }
  }
}
