package no.hassan.intellij.plugins.mobtimer;

public final class MobUtils {

  private MobUtils() {
    throw new RuntimeException("");
  }

  public static Tipple<Long, Long, Long> toDaysHoursAndMinutes(long seconds) {
    long hours = seconds / 3600;
    long days = hours / 24;
    if (days > 0) {
      hours = hours - days * 24;
    }
    long minutes = (seconds - ((seconds / 3600) * 3600)) / 60;
    return Tipple.of(days, hours, minutes);
  }

  public static Pair<Long, Long> toHoursAndMinutes(long seconds) {
    long hours = seconds / 3600;
    long minutes = (seconds - (hours * 3600)) / 60;
    return Pair.of(hours, minutes);
  }

  public static Pair<Integer, Byte> toMinutesAndSeconds(long milliseconds) {
    int minutes = (int) ((milliseconds / 1000) / 60);
    byte seconds = (byte) ((milliseconds / 1000) % 60);
    return Pair.of(minutes, seconds);
  }
}
