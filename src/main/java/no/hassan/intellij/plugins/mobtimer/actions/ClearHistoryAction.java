package no.hassan.intellij.plugins.mobtimer.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import no.hassan.intellij.plugins.mobtimer.MobbingManager;
import org.jetbrains.annotations.NotNull;

import java.awt.event.ActionEvent;

import static no.hassan.intellij.plugins.mobtimer.Constants.UI_Texts.MSG_CLEAR;

public class ClearHistoryAction extends AbstractAction {

  public ClearHistoryAction(@NotNull Project project, @NotNull MobbingManager mobbingManager) {
    super(project, mobbingManager);
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    int nowWhat = showOkCancelDialog(MSG_CLEAR);
    if (nowWhat == Messages.OK) {
      manager.flashAll();
    }
  }
}
