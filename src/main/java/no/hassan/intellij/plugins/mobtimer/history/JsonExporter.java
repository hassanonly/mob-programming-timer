package no.hassan.intellij.plugins.mobtimer.history;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonWriter;
import no.hassan.intellij.plugins.mobtimer.Model;
import org.jetbrains.annotations.NotNull;

import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class JsonExporter implements Exporter {

  @Override
  public boolean export(@NotNull OutputStream stream, @NotNull Model model) {
    try (JsonWriter writer = new JsonWriter(new OutputStreamWriter(stream))) {
      JsonObject root = new JsonObject();
      root.addProperty("interval", model.getInterval());
      root.addProperty("totalMobbingInSeconds", model.getTotalMobbingInSeconds());
      JsonArray mobbers = new JsonArray(model.getMobbers().size());
      model.getMobbers().stream()
          .map(
              mobber -> {
                JsonObject jo = new JsonObject();
                jo.addProperty("name", mobber.getName());
                jo.addProperty("turns", mobber.getTurnCount());
                jo.addProperty("skips", mobber.getSkipCount());
                jo.addProperty("left", mobber.isLeft());
                return jo;
              })
          .forEach(mobbers::add);
      root.add("mobbers", mobbers);
      writer.setIndent(" ");
      Gson gson = new Gson();
      gson.toJson(root, writer);
      writer.flush();
      return true;
    } catch (Exception ex) {
      String message = "Unable to history for reason: " + ex.getMessage();
      logger.error(message, ex);
      return false;
    }
  }
}
