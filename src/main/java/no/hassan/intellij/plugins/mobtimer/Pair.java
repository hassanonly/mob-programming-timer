package no.hassan.intellij.plugins.mobtimer;

import java.util.Objects;

public class Pair<P1, P2> {

  public final P1 p1;
  public final P2 p2;

  private Pair(P1 p1, P2 p2) {
    this.p1 = p1;
    this.p2 = p2;
  }

  public static <P1, P2> Pair<P1, P2> of(P1 p1, P2 p2) {
    return new Pair<>(p1, p2);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Pair<?, ?> tipple = (Pair<?, ?>) o;
    return p1.equals(tipple.p1) && p2.equals(tipple.p2);
  }

  @Override
  public int hashCode() {
    return Objects.hash(p1, p2);
  }

  @Override
  public String toString() {
    return "Pair{" + "p1=" + p1 + ", p2=" + p2 + '}';
  }
}
