package no.hassan.intellij.plugins.mobtimer;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Vector;

import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.ADDED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CHANGED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.CLEAR;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.REMOVED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.RESTARTED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.SHUFFLED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.SKIPPED;
import static no.hassan.intellij.plugins.mobtimer.Constants.MobbingEvents.STOPPED;

public class MobbersTableModel extends DefaultTableModel
    implements TableModelListener, PropertyChangeListener {

  private static final int COLUMN_NAME = 0;
  private static final int COLUMN_TURN = 1;
  private static final int COLUMN_SKIP = 2;
  private static final int COLUMN_GONE = 3;
  private static final long serialVersionUID = -2707852264405492749L;

  private final MobbingManager model;
  private final Class<?>[] columnClasses;

  MobbersTableModel(MobbingManager model, Vector<String> columnNames) {
    super(columnNames, 0);
    columnClasses = new Class[] {String.class, Integer.class, Integer.class, Boolean.class};
    this.model = model;
    model.addPropertyChangeListener(this);
    model.getMobbers().forEach(this::addMobber);
    addTableModelListener(this);
  }

  private void removeMobber(int rowIndex) {
    removeRow(rowIndex);
  }

  private void addMobber(Mobber mobber) {
    addRow(mobber.toDataRow());
  }

  @Override
  public boolean isCellEditable(int rowIndex, int columnIndex) {
    return columnIndex != 1 && columnIndex != 2;
  }

  @Override
  public Class<?> getColumnClass(int columnIndex) {
    return columnClasses[columnIndex];
  }

  @Override
  public void tableChanged(TableModelEvent e) {
    if (e.getType() == TableModelEvent.UPDATE) {
      int column = e.getColumn();
      int firstRow = e.getFirstRow();
      if (column == COLUMN_NAME) {
        model.nameUpdated(firstRow, (String) getValueAt(firstRow, column));
      } else if (column == COLUMN_GONE) {
        model.statusUpdated(firstRow, (Boolean) getValueAt(firstRow, column));
      }
    }
  }

  private void shuffle(List<Mobber> mobbers) {
    for (int row = 0; row < getRowCount(); row++) {
      for (int column = 0; column < getColumnCount(); column++) {
        Mobber mobber = mobbers.get(row);
        switch (column) {
          case COLUMN_NAME:
            setValueAt(mobber.getName(), row, column);
            break;
          case COLUMN_TURN:
            setValueAt(mobber.getTurnCount(), row, column);
            break;
          case COLUMN_SKIP:
            setValueAt(mobber.getSkipCount(), row, column);
            break;
          case COLUMN_GONE:
            setValueAt(mobber.isLeft(), row, column);
        }
      }
    }
  }

  private void updateMobber(Mobber mobber) {
    if (mobber != null) {
      int row = model.indexOf(mobber);
      setValueAt(mobber.getName(), row, COLUMN_NAME);
      setValueAt(mobber.getTurnCount(), row, COLUMN_TURN);
      setValueAt(mobber.getSkipCount(), row, COLUMN_SKIP);
      setValueAt(mobber.isLeft(), row, COLUMN_GONE);
    }
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    String propertyName = evt.getPropertyName();
    switch (propertyName) {
      case ADDED:
        addMobber((Mobber) evt.getNewValue());
        break;
      case REMOVED:
        removeMobber((int) evt.getOldValue());
        break;
      case SHUFFLED:
        //noinspection unchecked
        shuffle((List<Mobber>) evt.getNewValue());
        break;
      case CHANGED:
        updateMobber((Mobber) evt.getNewValue());
        break;
      case RESTARTED:
      case SKIPPED:
        Mobber mobber = (Mobber) evt.getOldValue();
        int skippIndex = model.indexOf(mobber);
        setValueAt(mobber.getTurnCount(), skippIndex, COLUMN_TURN);
        setValueAt(mobber.getSkipCount(), skippIndex, COLUMN_SKIP);
        break;
      case CLEAR:
        getDataVector().clear();
        break;
      case STOPPED:
        fireTableCellUpdated(0, 0);
        break;
    }
  }
}
