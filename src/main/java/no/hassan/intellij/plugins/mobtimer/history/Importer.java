package no.hassan.intellij.plugins.mobtimer.history;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.intellij.openapi.diagnostic.Logger;
import no.hassan.intellij.plugins.mobtimer.Model;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Path;

public interface Importer {

  Logger logger = Logger.getInstance(Importer.class);

  static Model getDefault() {
    JsonObject mJson = new JsonObject();
    mJson.addProperty("interval", 10);
    mJson.addProperty("totalMobbingInSeconds", 0);
    mJson.add("mobbers", new JsonArray());
    return new Model(mJson);
  }

  Model load(@NotNull Path mobFilePath);
}
