package no.hassan.intellij.plugins.mobtimer.history;

import static java.lang.Thread.currentThread;
import static java.util.Objects.requireNonNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import no.hassan.intellij.plugins.mobtimer.Model;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Verifying the behavior of JsonImporter implementation")
class JsonImporterTests {
  private JsonImporter importer = new JsonImporter();

  @Test
  @DisplayName("If file exists, its loaded correctly")
  void whenFileExists() throws URISyntaxException {
    Model model =
        importer.load(
            Paths.get(
                requireNonNull(currentThread().getContextClassLoader().getResource(".mob.json"))
                    .toURI()));
    assertAll(
        () -> assertNotNull(model),
        () -> assertEquals(15, model.getInterval()),
        () -> assertEquals(1552L, model.getTotalMobbingInSeconds()),
        () -> assertEquals("Mohammad Ali", model.getMobbers().get(0).getName()),
        () -> assertEquals("Eliza", model.getMobbers().get(1).getName()),
        () -> assertEquals("Ayesha", model.getMobbers().get(2).getName()),
        () -> assertEquals("Nazia", model.getMobbers().get(3).getName()),
        () -> assertEquals("Hassan", model.getMobbers().get(4).getName()));
  }

  @DisplayName("If no file exists then a default Model is returned")
  @Test
  void noFileExists() {
    Model model = importer.load(Paths.get("nothing-here.fcuk"));
    assertAll(
        () -> assertNotNull(model),
        () -> assertEquals(10, model.getInterval()),
        () -> assertEquals(0, model.getTotalMobbingInSeconds()),
        () -> assertTrue(model.getMobbers().isEmpty()));
  }
}
