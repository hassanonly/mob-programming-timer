package no.hassan.intellij.plugins.mobtimer;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sun.tools.javac.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Assures that JsonObject is parsed correctly")
class ModelTests {

  @DisplayName("Building a Model from valid JsonObject shall return new Model")
  @Test
  void constructFromJsonObject() {
    JsonObject json = new JsonObject();
    json.addProperty("interval", 15);
    json.addProperty("totalMobbingInSeconds", 100_00_00L);
    Mobber loadedMobber = new Mobber("Loaded");
    loadedMobber.setSkipCount(10);
    loadedMobber.setTurnCount(200);
    List<Mobber> mobbers = List.of(new Mobber("A"), new Mobber("B"), loadedMobber);
    JsonArray jsonMobbers = new JsonArray();
    mobbers.forEach(mobber -> jsonMobbers.add(mobber.toJson()));
    json.add("mobbers", jsonMobbers);
    Model model = new Model(json);
    assertAll(
        () -> assertNotNull(model),
        () -> assertEquals(15, model.getInterval()),
        () -> assertEquals(100_00_00L, model.getTotalMobbingInSeconds()),
        () -> assertEquals(mobbers, model.getMobbers()));
  }
}
