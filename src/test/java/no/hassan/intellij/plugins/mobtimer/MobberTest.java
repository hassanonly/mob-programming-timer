package no.hassan.intellij.plugins.mobtimer;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;

import com.google.gson.JsonObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@SuppressWarnings("OptionalGetWithoutIsPresent")
@DisplayName("The Mobber class have logic which can parse JsonObject")
class MobberTest {

  @DisplayName("toJson creates a JsonObject")
  @Test
  void toJson() {
    Mobber mobber = new Mobber("Tester@:!null{\"\"\"}");
    mobber.setTurnCount(10);
    mobber.setSkipCount(11);
    JsonObject json = mobber.toJson();
    assertAll(
        () -> assertNotNull(json),
        () -> assertEquals(mobber.getName(), json.get("name").getAsString()),
        () -> assertFalse(json.get("left").getAsBoolean()),
        () -> assertEquals(mobber.getTurnCount(), json.get("turns").getAsInt()),
        () -> assertEquals(mobber.getSkipCount(), json.get("skips").getAsInt()));
  }

  @DisplayName("A JsonObject shall be parsed to Mobber object")
  @Test
  void parse() {
    JsonObject json = new JsonObject();
    json.addProperty("name", "Tester");
    json.addProperty("turns", 10);
    json.addProperty("skips", 11);
    json.addProperty("left", true);
    Optional<Mobber> mobber = Mobber.parse(json);
    assertAll(
        () -> assertTrue(mobber.isPresent()),
        () -> assertEquals("Tester", mobber.get().getName()),
        () -> assertEquals(10, mobber.get().getTurnCount()),
        () -> assertEquals(11, mobber.get().getSkipCount()),
        () -> assertTrue(mobber.get().isLeft()));
  }

  @DisplayName("It should be possible to parse if name is still present")
  @Test
  void parseIncomplete() {
    JsonObject json = new JsonObject();
    json.addProperty("name", "Tester");
    Optional<Mobber> mobber = Mobber.parse(json);
    assertAll(
        () -> assertTrue(mobber.isPresent()),
        () -> assertEquals("Tester", mobber.get().getName()),
        () -> assertEquals(0, mobber.get().getTurnCount()),
        () -> assertEquals(0, mobber.get().getSkipCount()),
        () -> assertFalse(mobber.get().isLeft()));
  }

  @DisplayName("JsonObject without name is not parsable")
  @Test
  void unParsableIfNameNotPresent() {
    JsonObject json = new JsonObject();
    json.addProperty("turns", 10);
    json.addProperty("skips", 11);
    json.addProperty("left", true);
    Optional<Mobber> mobber = Mobber.parse(json);
    assertFalse(mobber.isPresent());
  }
}
